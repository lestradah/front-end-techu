
function agregarFactura(){

    document.getElementById("facturaFound").innerText = "";

    var factura = new Object();
    factura.idFactura = document.getElementById("idFactura").value;
    factura.fecFactura = document.getElementById("fecFactura").value;
    factura.totalFactura = document.getElementById("totalFactura").value;

    var jsonFactura = localStorage.getItem(factura.idFactura);

    if(jsonFactura !== null) {
        document.getElementById("facturaFound").innerText = "Factura ya existente!!!";
    } else{
        localStorage.setItem(factura.idFactura, JSON.stringify(factura));
        document.getElementById("facturaFound").innerText = "Factura agregada";
    }
}





function buscarFactura (){

    document.getElementById("facturaFound").innerText = "";

    var idFactura = document.getElementById("idFactura").value;
    var facturaFound = localStorage.getItem(idFactura);
    var msg = document.getElementById("facturaFound");

    if(facturaFound !== null){

        msg.innerText = "Factura encontrada " + facturaFound;

    } else {

        msg.innerText = "Factura no encontrada!!!";
    }


}

function eliminarFactura(){

    document.getElementById("facturaFound").innerText = "";

    var idFactura = document.getElementById("idFactura").value;

    var facturaFound = JSON.parse(localStorage.getItem(idFactura));

    if(facturaFound  !== null){
        localStorage.removeItem(idFactura);
        document.getElementById("facturaFound").innerText = "Factura eliminada " + facturaFound.idFactura;
    }


    //confirming


}

function limpiarFacturas(){

    document.getElementById("facturaFound").innerText = "";

    localStorage.clear();
    document.getElementById("facturaFound").innerText = "Facturas eliminadas";


    //confirming

}